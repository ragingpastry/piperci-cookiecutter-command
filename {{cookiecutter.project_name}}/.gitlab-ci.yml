---
stages:
  - lint
  - sanity
  - unit
  - build
  - generate_release
  - release

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  GATEWAY_IMAGE: ${CI_REGISTRY_IMAGE}/gateway
  EXECUTOR_IMAGE: ${CI_REGISTRY_IMAGE}/executor


commitchecker:
  image: registry.gitlab.com/dreamer-labs/dl-commitlint:latest
  stage: lint
  only:
    - merge_requests
  script:
    - git fetch origin ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME};
    - git fetch origin ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME};
    - commitlint --from=origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} --to=origin/${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME};


lint:
  stage: lint
  image: python:3.7
  before_script:
    - pip install tox
  script:
    tox -e lint
  only:
    - merge_requests
    - master
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^chore\(release\)/

sanity:
  stage: sanity
  image: python:3.7
  before_script:
    - pip install tox
  script:
    tox -e sanity
  only:
    - merge_requests
    - master
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^chore\(release\)/

unit:
  stage: unit
  image: python:3.7
  before_script:
    - pip install tox
  script:
    tox -e unittest
  only:
    - merge_requests
    - master
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^chore\(release\)/

build:
  services:
    - docker:18.09.7-dind
  image: docker:18-git
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - apk add curl
    - curl -sSL https://cli.openfaas.com | sh
    - GATEWAY_TEST_IMAGE=$GATEWAY_IMAGE:$CI_COMMIT_REF_SLUG
    - EXECUTOR_TEST_IMAGE=$EXECUTOR_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - faas-cli template pull https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates
    - faas-cli build --shrinkwrap
    - docker build -t $GATEWAY_TEST_IMAGE -f build/piperci-{{cookiecutter.command_name}}-gateway/Dockerfile build/piperci-{{cookiecutter.command_name}}-gateway
    - docker build -t $EXECUTOR_TEST_IMAGE -f build/piperci-{{cookiecutter.command_name}}-executor/Dockerfile build/piperci-{{cookiecutter.command_name}}-executor
    - docker push $GATEWAY_TEST_IMAGE
    - docker push $EXECUTOR_TEST_IMAGE
  only:
    - merge_requests
    - master

generate_release:
  stage: generate_release
  services:
    - docker:18.09.7-dind
  image: registry.gitlab.com/dreamer-labs/dl-semantic-release:latest
  script:
    - mkdir ~/.ssh && chmod 0700 ~/.ssh
    - echo "$DEPLOY_KEY" > ~/.ssh/id_rsa
    - chmod 0600 ~/.ssh/id_rsa
    - ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
    - eval `ssh-agent`
    - ssh-add
    - semantic-release --repository-url git@gitlab.com:${CI_PROJECT_PATH}.git
  only:
    refs:
      - master
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^chore/
    refs:
      - tags

release-image:
  services:
    - docker:18.09.7-dind
  image: docker:18-git
  stage: release
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - tag=$(cat VERSION)
    - GATEWAY_TEST_IMAGE=$GATEWAY_IMAGE:$CI_COMMIT_REF_SLUG
    - EXECUTOR_TEST_IMAGE=$EXECUTOR_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - GATEWAY_RELEASE_IMAGE=${GATEWAY_IMAGE}:$tag
    - EXECUTOR_RELEASE_IMAGE=${EXECUTOR_IMAGE}:$tag
    - docker pull $GATEWAY_TEST_IMAGE
    - docker pull $EXECUTOR_TEST_IMAGE
    - docker tag $GATEWAY_TEST_IMAGE $GATEWAY_RELEASE_IMAGE
    - docker tag $EXECUTOR_TEST_IMAGE $EXECUTOR_RELEASE_IMAGE
    - docker tag $GATEWAY_TEST_IMAGE ${GATEWAY_IMAGE}:latest
    - docker tag $EXECUTOR_TEST_IMAGE ${EXECUTOR_IMAGE}:latest
    - docker push $GATEWAY_RELEASE_IMAGE
    - docker push $EXECUTOR_RELEASE_IMAGE
    - docker push ${GATEWAY_IMAGE}:latest
    - docker push ${EXECUTOR_IMAGE}:latest
  only:
    refs:
      - master
    variables:
      - $CI_COMMIT_MESSAGE =~ /^chore\(release\)/
